FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV HDF5_USE_FILE_LOCKING=FALSE


    # Install common dependencies
    RUN apt-get update \
      && apt-get --yes install --no-install-recommends \
      bison \
      build-essential \
      cmake \
      flex \
      gfortran \
      git \
      libblas-dev \
      libcfitsio-dev \
      libfftw3-dev \
      libhdf5-serial-dev \
      liblapacke-dev \
      libpng-dev \
      libpython3-dev \
      python-setuptools \
      libncurses5-dev \
      libreadline-dev \
      libxml2-dev \
      pybind11-dev \
      python3-dev \
      python3-pip \
      wcslib-dev \
      wget \
      libarmadillo-dev \
      liblua5.3-dev \
      libboost-date-time-dev \
      libboost-numpy-dev \
      libboost-python-dev \
      libboost-test-dev \
      libboost-system-dev \
      libboost-filesystem-dev \
      libboost-program-options-dev \
      libboost-test-dev \
      libgtkmm-3.0-dev \
      libgsl-dev \
      subversion \
      liblog4cplus-dev \
      && rm -rf /var/lib/apt/lists/*
    
    RUN pip3 install numpy
    
    # 
    RUN mkdir /opt/lofarsoft/
    
    # Install casacore data
    RUN mkdir -p /opt/lofarsoft/data \
    && cd /opt/lofarsoft/data \
    && wget ftp://anonymous@ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar \
    && tar xvf WSRT_Measures.ztar \
    && rm WSRT_Measures.ztar
    
    # Build casacore
    RUN cd / & wget https://github.com/casacore/casacore/archive/v3.3.0.tar.gz \
    && tar xvf v3.3.0.tar.gz && cd casacore-3.3.0 \
    && mkdir build && cd build \
    && cmake \
        -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft/ \
        -DDATA_DIR=/opt/lofarsoft/data \
        -DUSE_OPENMP=ON -DUSE_THREADS=OFF -DUSE_FFTW3=TRUE \
        -DUSE_HDF5=ON -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_PYTHON=OFF \
        -DBUILD_PYTHON3=ON -DPYTHON3_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.8.so \
        -DPYTHON3_INCLUDE_DIR=/usr/include/python3.8/ -DPYTHON3_EXECUTABLE=/usr/bin/python3 \
        -DCMAKE_CXX_FLAGS="-fsigned-char -O2 -DNDEBUG" ../ \
    && make -j4 && make install && cd / && rm -rf v3.3.0.tar.gz casacore-3.3.0
    
    # Install python casacore
    RUN cd / & wget https://github.com/casacore/python-casacore/archive/v3.3.1.tar.gz \
    && tar xvf v3.3.1.tar.gz && cd python-casacore-3.3.1 \
    && mkdir -p /opt/lofarsoft/lib/python3.8/site-packages/ \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && export LD_LIBRARY_PATH=/opt/lofarsoft/lib \
    && export PATH=/opt/lofarsoft/include:${PATH} \
    && python3 ./setup.py build_ext -I/opt/lofarsoft/include -L/opt/lofarsoft/lib \
    && python3 ./setup.py install --prefix=/opt/lofarsoft/ \
    && cd / && rm -rf python-casacore-3.3.1 v3.3.1.tar.gz
    
    # Install aoflagger 3.0
    RUN wget https://gitlab.com/aroffringa/aoflagger/-/archive/v3.0.0/aoflagger-v3.0.0.tar.bz2 \
    && tar xvf aoflagger-v3.0.0.tar.bz2 \
    && cd aoflagger-v3.0.0 && mkdir build && cd build \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft/ \
             -DPYTHON_EXECUTABLE=/usr/bin/python3 \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft ../ \
    && make -j8 && make install && cd / && rm -rf aoflagger-v3.0.0.tar.bz2 aoflagger-v3.0.0

    # Install the LOFAR Beam Library v4.1.1
    RUN git clone https://github.com/lofar-astron/LOFARBeam.git \
    && cd LOFARBeam \
    && mkdir build && cd build \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft/ \
             -DPYTHON_EXECUTABLE=/usr/bin/python3 \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft/ ../ \
    && make && make install && cd / && rm -rf LOFARBeam
    
    # Install IDG
    RUN cd / && git clone https://gitlab.com/astron-idg/idg.git \
    && cd idg && mkdir build && cd build \
    && cmake -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft/ ../ \
    && make && make install && cd / && rm -rf idg
    
    # Install wsclean
    RUN cd / && wget https://gitlab.com/aroffringa/wsclean/-/archive/v2.10/wsclean-v2.10.tar.bz2 \
    && tar xvf wsclean-v2.10.tar.bz2 && cd wsclean-v2.10 && cd wsclean \
    && mkdir build && cd build \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft \
             -DIDGAPI_INCLUDE_DIRS=/opt/lofarsoft/include \
             -DCMAKE_PREFIX_PATH=/opt/lofarsoft \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft ../ \
    && make -j4 && make install && cd / && rm -rf wsclean-v2.10 wsclean-v2.10.tar.bz2
    
    # Install dysco
    RUN cd / && wget https://github.com/aroffringa/dysco/archive/v1.2.tar.gz \
    && tar xvf v1.2.tar.gz && cd dysco-1.2 && mkdir build && cd build \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft ../ \
    && make && make install && cd / && rm -rf v1.2.tar.gz dysco-1.2
    
    # Install losoto
    RUN cd / && wget https://github.com/revoltek/losoto/archive/2.0.tar.gz \
    && tar xvf 2.0.tar.gz && cd losoto-2.0 \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && python3 setup.py install --prefix=/opt/lofarsoft/ && cd / && rm -rf losoto
    
    # Install pyBDSF
    #RUN wget https://github.com/lofar-astron/PyBDSF/archive/v1.9.2.tar.gz \
    #&& tar xvf v1.9.2.tar.gz && cd PyBDSF-1.9.2 \
    RUN git clone https://github.com/lofar-astron/PyBDSF.git \
    && cd PyBDSF \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && python3 setup.py install --prefix=/opt/lofarsoft/ \
    && cd / && rm -rf v1.9.2.tar.gz PyBDSF-1.9.2
    
    # Install LSMTool
    RUN cd / && wget https://github.com/darafferty/LSMTool/archive/v1.4.2.tar.gz \
    && tar xvf v1.4.2.tar.gz && cd LSMTool-1.4.2 \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && python3 setup.py install --prefix=/opt/lofarsoft/ \
    && cd / && rm -rf LSMTool-1.4.2 v1.4.2.tar.gz
    
    # Install RMextract
    RUN cd / && git clone https://github.com/lofar-astron/RMextract.git \
    && cd RMextract \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && python3 setup.py install --prefix=/opt/lofarsoft && cd / && rm -rf RMextract
    
    # Install LofarStMan
    RUN cd / && git clone https://github.com/lofar-astron/LofarStMan.git \
    && cd LofarStMan && mkdir -p build && cd build \
    && cmake -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft/ \
             -DCMAKE_PREFIX_PATH=/opt/lofarsoft ../ \
    && make && make install \
    && cd / && rm -rf LofarStMan
    
    # Install Everybeam
    RUN cd / && git clone https://git.astron.nl/RD/EveryBeam.git \
    && cd EveryBeam && mkdir build && cd build \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft/ \
             -DCMAKE_PREFIX_PATH=/opt/lofarsoft ../ \
    && make && make install \
    && cd / && rm -rf EveryBeam
    
    # Install DP3 master
    RUN cd / && git clone https://github.com/lofar-astron/DP3.git \
    && cd DP3 && mkdir build && cd build \
    && cmake -DCASACORE_ROOT_DIR=/opt/lofarsoft/ \
             -DAOFLAGGER_ROOT=/opt/lofarsoft/ \
             -DIDGAPI_LIBRARIES=/opt/lofarsoft/lib/libidg-api.so \
             -DIDGAPI_INCLUDE_DIRS=/opt/lofarsoft/include \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft/ ../ \
    && make -j8 && make install \
    && ln -s /opt/lofarsoft/bin/DPPP /opt/lofarsoft/bin/NDPPP \
    && cd / && rm -rf DP3 
    
    # Install LOFAR 4.0
    RUN cd / \
    && svn --non-interactive -q co \
      https://svn.astron.nl/LOFAR/branches/LOFAR-Release-4_0/ source 
    RUN pip3 install xmlrunner
    RUN cd / && mkdir -p source/build/gnucxx11_optarch \
    && cd source/build/gnucxx11_optarch \
    && export PYTHONPATH=/opt/lofarsoft//lib/python3.8/site-packages/ \
    && cmake -DBUILD_PACKAGES="Pipeline ParmDB pyparmdb" \
             -DCASACORE_ROOT_DIR=/opt/lofarsoft \
             -DPYTHON_EXECUTABLE=/usr/bin/python3 \
             -DUSE_OPENMP=True \
             -DBUILD_TESTING=OFF \
             -DCMAKE_INSTALL_PREFIX=/opt/lofarsoft \
             -DBUILD_TESTING=OFF -DUSE_OPENMP=True ../../ \
    && make -j1 && make install && cd / && rm -rf source
